# Инструменты

В данной главе описаны инструменты для поиска кандидатов. Примеры рабочие взяты из практики, можно взять любой инструмент или ресурс и начать использовать. Однако, здесь не описывается методика как эти инструменты следует интегрировать в другие рабочие процессы. Отчасти это будет написано в главе "Стратегия". Кроме этого инструменты не все, некоторые будут описаны в следующих главах по причине того, что их так лучше описать и следовательно понять.

## Содержание главы:

1. [LinkedIn](https://hr-notes.gocareera.ru/linkedin.html)
2. [Мой Круг](https://hr-notes.gocareera.ru/moi-krug.html)
3. [GitHub](https://hr-notes.gocareera.ru/github.html)
4. [StackOverflow](https://hr-notes.gocareera.ru/stackoverflow.html)
5. [CSE Google](https://hr-notes.gocareera.ru/cse_google.html)
6. [Скрипты (scripts)](https://hr-notes.gocareera.ru/scripts.html)

## Используемые технологии в статьях:

- Операционная Система - любая, но примеры сделаны на Linux.
- Браузер - любой.
- ПО:
	- Python (https://www.python.org/)
	- R (https://www.r-project.org/)
	- R-Studio (https://www.rstudio.com/products/rstudio/download/#download)
	- Скрипты (https://gitlab.com/DmitriyBerg/recruiting-tools)
	- Terminal/Console (Терминал) - любой.
- Зарегистрированные аккаунты в: Моём Круге, LinkedIn, GitHub, Google.
