# Вопросы к iOS разработчикам (middle/senior)

## Источники

- https://habr.com/ru/post/198612/

## Вопросы

### General

1. Чем абстрактный класс отличается от интерфейса?
2. Расскажите о паттерне MVC. Чем отличается пассивная модель от активной?
3. Реализация синглтона в ARC и в non-ARC?
4. Какие еще паттерны знаете?
5. Напишите код, который разворачивает строку на С++.

### Networking & Multithreading

1. Что такое deadlock?
2. Что такое livelock?
3. Что такое семафор?
4. Что такое мьютекс?
5. Асинхронность vs многопоточность. Чем отличаются?
6. Преимущества и недостатки синхронного и асинхронного соединения?
7. Что означает http, tcp?
8. Какие различия между HEAD, GET, POST, PUT?
9. Какие технологии в iOS возможно использовать для работы с потоками. Преимущества и недостатки.
10. Чем отличается dispatch_async от dispatch_sync?
11. Выведется ли в дебагер «Hello world»? Почему?

```objc
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
     dispatch_sync(dispatch_get_main_queue(), ^{
         NSLog(@"Hello world");
     });

    /* Another implementation */
    return YES;
}
```

12. Что выведется в консоль?

```objc
	NSObject *object = [NSObject new];
	dispatch_async(dispatch_get_main_queue(), ^
	{
		NSLog(@"A %d", [object retainCount]);
		dispatch_async(dispatch_get_main_queue(), ^
		{
			NSLog(@"B %d", [object retainCount]);
		});
		NSLog(@"C %d", [object retainCount]);
	});
	NSLog(@"D %d", [object retainCount]);
```

### CoreData

1. Что такое Core Data?
2. В каких случаях лучше использовать SQLite, а в каких Core Data?
3. Что такое Managed object context?
4. Что такое Persistent store coordinator?
5. Какие есть нюансы при использовании Core Data в разных потоках? Как синхронизировать данные между потоками?
6. Использовали ли NSFetchedResultsController? Почему?

### Objective-C

1. Какие существуют root классы в iOS? Для чего нужны root классы?
2. Что такое указатель isa? Для чего он нужен?
3. Что происходит с методом после того, как он не нашелся в объекте класса, которому его вызвали?
4. Чем категория отличается от расширения (extention, неименованная категория)?
5. Можно ли добавить ivar в категорию?
6. Когда лучше использовать категорию, а когда наследование?
7. Какая разница между использованием делагатов и нотификейшенов?
8. Как происходит ручное управление памятью в iOS?
9. autorelease vs release?
10. Что означает ARC?
11. Что делать, если проект написан с использованием ARC, а нужно использовать классы сторонней библиотеки написанной без ARC?
12. Weak vs assign, strong vs copy?
13. Atomic vs nonatomic. Чем отличаются? Как вручную переопределить atomic/nonatomic сеттер в не ARC коде?
14. Зачем все свойства ссылающиеся на делегаты strong/retain. :)))
15. Что такое autorelease pool?
16. Как можно заимплементировать autorelease pool на с++?
17. Чем отличается NSSet от NSArray? Какие операции быстро происходят в NSSet и какие в NSArray?
18. Formal vs informal protocol.
19. Есть ли приватные или защищенные методы в Objective-C?
20. Как имитировать множественное наследование?
21. Что такое KVO? Когда его нужно использовать?
22. Что такое KVC? Когда его нужно использовать?
23. Что такое блоки? Зачем они нужны?
24. Когда нужно копировать блок? Кто за это ответственен: caller или reciever?
25. Что такое designated initializer?
26. Что не так с этим кодом? Зачем нужны инициализаторы?

```
[[[SomeClass alloc] init] init];
```

27. Как удалить объект в ходе итерации по циклу?
28. Сработает ли таймер? Почему?

```objc
void startTimer(void *threadId)
{
   [NSTimer  scheduleTimerWithTimeInterval:10.0f
		target:aTarget
			selector:@selector(tick: )
			userInfo:nil
				repeats:NO];
}
pthread_create(&thread, NULL, startTimer, (void *)t);
```

29. Какой метод вызовется: класса A или класса B? Как надо изменить код, чтобы вызвался метод класса A?

```objc
@interface A : NSObject

- (void)someMethod;
@end

@implementation A

- (void)someMethod
{
    NSLog(@"This is class A");
}
@end

@interface B : A
@end

@implementation B

- (void)someMethod
{
    NSLog(@"This is class B");
}
@end

@interface C : NSObject
@end

@implementation C

- (void)method
{
    A *a = [B new];
    [a someMethod];
}
@end
```

30. В каких случаях лучше использовать strong, а в каких copy для NSString? Почему?

```objc
@property (nonatomic, strong) NSString *someString;
@property (nonatomic, copy) NSString *anotherString;
```

31. Что выведется в консоль? Почему?

```objc
- (BOOL)objectsCount
{
    NSMutableArray *array = [NSMutableArray new];
    for (NSInteger i = 0; i < 1024; i++)
    {
        [array addObject:[NSNumber numberWithInt:i]];
    }
    return array.count;
}

- (void)someMethod
{
    if ([self objectsCount])
    {
        NSLog(@"has objects");
    }
    else
    {
        NSLog(@"no objects");
    }
}>)}
```

### UIKit

1. Что такое Run Loop?
2. Чем отличается frame от bounds?
3. Что такое responder chain?
4. Если я вызову performSelector:withObject:afterDelay: – объекту пошлется сообщение retain?
5. Какие бывают состояния у приложения?
6. Как работают push нотификации?
7. Цикл жизни UIViewController?
8. Как происходит обработка memory warning? Зависит ли обработка от версии iOS?
9. Как лучше всего загрузить UIImage c диска(с кеша)?
10. Какой контент лучше хранить в Documents, а какой в Cache?
